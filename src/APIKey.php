<?php
/**
 * This file is part of the Aspetos API Client library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) cwd.at GmbH <office@cwd.at>
 */
namespace Aspetos\ApiClient;

use League\OAuth2\Client\Grant\AbstractGrant;

/**
 * Represents a client credentials grant.
 *
 * @link http://tools.ietf.org/html/rfc6749#section-1.3.4 Client Credentials (RFC 6749, §1.3.4)
 */
class APIKey extends AbstractGrant
{
    /**
     * @inheritdoc
     */
    protected function getName()
    {
        return 'http://aspetos.com/grants/api_key';
    }

    /**
     * @inheritdoc
     */
    protected function getRequiredRequestParameters()
    {
        return ['api_key'];
    }
}
