<?php
namespace Aspetos\ApiClient;


use League\OAuth2\Client\Grant\GrantFactory;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;

class Client
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * @var array
     */
    private $config = [];


    /**
     * @var AccessToken|null
     */
    private $token = null;

    /**
     * @var null
     */
    private $provider = null;

    /**
     * NeosClient constructor.
     *
     * @param array $config
     */
    public function __construct($config)
    {
        $this->config = $config;

        $this->provider = new GenericProvider([
            'clientId'                => $this->getConfig('clientId'),
            'clientSecret'            => $this->getConfig('clientSecret'),
            'urlAuthorize'            => $this->getConfig('authUrl'),
            'urlAccessToken'          => $this->getconfig('tokenUrl'),
            'urlResourceOwnerDetails' => $this->getConfig('baseUri').'me',
        ]);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getAccessToken()
    {
        if ($this->token instanceof AccessToken) {
            $expire = new \DateTime('@'.$this->token->getExpires());
            if ($expire < new \DateTime()) {
                return $this->refreshToken();
            }

            return $this->token->getToken();
        }

        try {
            $grantFactory = new GrantFactory();
            $grantFactory->setGrant('http://aspetos.com/grants/api_key', new APIKey());
            $this->provider->setGrantFactory($grantFactory);
            $this->token = $this->provider->getAccessToken('http://aspetos.com/grants/api_key', array('api_key' => $this->getConfig('apiKey')));
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return $this->token->getToken();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function refreshToken()
    {
        if (!$this->token instanceof AccessToken) {
            $this->token = $this->provider->getAccessToken('refresh_token', array('refresh_token' => $this->token->getRefreshToken()));

            return $this->token->getToken();
        }

        throw new \Exception('No refresh token present');
    }

    /**
     * @param string $key
     *
     * @return string|array
     * @throws \Exception
     */
    private function getConfig($key)
    {
        if (!isset($this->config[$key])) {
            throw new \Exception(sprintf('Config key "%s" is not set', $key));
        }

        return $this->config[$key];
    }

    /**
     * @return \GuzzleHttp\Client|Client
     */
    private function getClient()
    {
        if ($this->client instanceof \GuzzleHttp\Client) {
            return $this->client;
        }

        $this->client = new \GuzzleHttp\Client(array(
            'base_uri' => $this->getConfig('baseUri'),
            'query' => array(
                'access_token' => $this->getAccessToken(),
                '_format' => 'json'
            )
        ));

        return $this->client;
    }



    /**
     * @param string $target
     * @param array  $params
     * @param string $method
     *
     * @return array
     */
    public function call($target, $params = array(), $method = 'GET')
    {
        $client = $this->getClient();
        if ($method == 'GET') {
            $response = $client->request($method, $target, array('query' => array_merge($client->getConfig('query'), $params)));
        } else {
            $response = $client->request($method, $target, array('multipart' => $params, 'query' => $client->getConfig('query')));
        }

        return json_decode($response->getBody(), true);
    }

}
