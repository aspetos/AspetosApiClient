Aspetos API Client
===============

This is a very small helper to utilise the Aspetos API v1

To install add this to your `composer.json`

```
{
    "repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.cwd.at:aspetos/AspetosApiClient.git",
            "tags-path": true,
            "branches-path": false
        },
    ],
    "require": {
        "aspetos/api-client": "@stable"
    }
}    
```

See index.php for an example on how to use it
